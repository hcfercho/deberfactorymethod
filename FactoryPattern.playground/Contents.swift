import XCTest

protocol Supplier {

    func factoryMethod() -> Product

    func someOperation() -> String
}

extension Supplier {

    func someOperation() -> String {
        // Call the factory method to create a Product object.
        let product = factoryMethod()

        // Now, use the product.
        return "Creator: The same creator's code has just worked with " + product.operation()
    }
}


class FoodSupplier: Supplier {

    public func factoryMethod() -> Product {
        return Snacks()
    }
}

class DrinkSupplier: Supplier {

    public func factoryMethod() -> Product {
        return Juice()
    }
}


protocol Product {

    func operation() -> String
}

/// Concrete Products provide various implementations of the Product protocol.
class Snacks: Product {

    func operation() -> String {
        return "{Result of the ConcreteProduct1}"
    }
}

class Juice: Product {

    func operation() -> String {
        return "{Result of the ConcreteProduct2}"
    }
}

class Client {
    
    static func someClientCode(creator: Supplier) {
        print("Client: I'm not aware of the creator's class, but it still works.\n"
            + creator.someOperation());
    }
    
}


class FactoryMethodConceptual: XCTestCase {

    func testFactoryMethodConceptual() {
        //Falta implementar los cases para gestionar la creacion de los suppliers.
        print("App: Launched with the FoodSupplier.");
        Client.someClientCode(creator: FoodSupplier())

        //print("\nApp: Launched with the DrinkSupplier.");
        //Client.someClientCode(creator: DrinkSupplier())
    }
}

FactoryMethodConceptual().testFactoryMethodConceptual()
